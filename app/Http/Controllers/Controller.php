<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    /**
     * Returns empty JSON response for home route.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return JsonResponse::create();
    }
}
