<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Routing\Registrar as RegistrarContract;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerDefaultRoutes($this->app[Router::class]);
    }

    /**
     * Registers default application routes.
     *
     * @param Router $router A Router instance.
     *
     * @return void
     */
    private function registerDefaultRoutes(Router $router): void
    {
        $attributes = [
            'middleware' => ['web', 'api'],
            'namespace' => 'App\Http\Controllers',
        ];

        $router->group($attributes, static function (RegistrarContract $router): void {
            $router->get('', 'Controller@index');
        });
    }
}
