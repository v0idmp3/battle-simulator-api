<?php

declare(strict_types=1);

namespace App\Games;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Attack extends Model
{
    /**
     * Eloquent relationship: Each attack has one attacker army.
     *
     * @return HasOne
     */
    public function attackerArmy(): HasOne
    {
        return $this->hasOne(Army::class, 'id', 'attacker_army_id');
    }

    /**
     * Eloquent relationship: Each attack may have defending army.
     *
     * @return HasOne
     */
    public function defendingArmy(): HasOne
    {
        return $this->hasOne(Army::class, 'id', 'defending_army_id');
    }
}
