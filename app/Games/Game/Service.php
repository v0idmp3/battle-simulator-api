<?php

declare(strict_types=1);

namespace App\Games\Game;

use App\Games\Army\Repository as ArmyRepository;
use App\Games\Attack\Repository as AttackRepository;
use App\Games\Game;
use App\Games\Game\Repository as GameRepository;
use Illuminate\Database\Eloquent\Collection;
use Ramsey\Uuid\Uuid;

class Service
{
    /**
     * A GameRepository instance.
     *
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * A ArmyRepository instance.
     *
     * @var ArmyRepository
     */
    private $armyRepository;

    /**
     * A AttackRepository instance.
     *
     * @var AttackRepository
     */
    private $attackRepository;

    /**
     * @param GameRepository   $gameRepository   A GameRepository instance.
     * @param ArmyRepository   $armyRepository   A ArmyRepository instance.
     * @param AttackRepository $attackRepository A AttackRepository instance.
     */
    public function __construct(
        GameRepository $gameRepository,
        ArmyRepository $armyRepository,
        AttackRepository $attackRepository
    ) {
        $this->gameRepository = $gameRepository;
        $this->armyRepository = $armyRepository;
        $this->attackRepository = $attackRepository;
    }

    /**
     * Returns all games in database.
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->gameRepository->getAllWithOrder();
    }

    /**
     * Create a new game and returns it.
     *
     * @param string $name The game name.
     *
     * @return Game
     */
    public function createNewGame(string $name): Game
    {
        $inputData = [
            'uuid' => Uuid::uuid4(),
            'name' => $name,
        ];

        $createdGame = $this->gameRepository->create($inputData);

        return $createdGame;
    }

    /**
     * Reset a game.
     *
     * @param Game $game A Game instance.
     *
     * @return void
     */
    public function resetGame(Game $game): void
    {
        $game->load([
            'armies',
        ]);

        $armyIds = $game->armies->pluck('id')->toArray();
        $this->armyRepository->resetArmies($armyIds);
        $this->attackRepository->deleteAttacksByArmies($armyIds);
    }
}
