<?php

declare(strict_types=1);

namespace App\Games\Game;

use App\Games\Game;
use Illuminate\Database\Eloquent\Collection;

class Serializer
{
    /**
     * Serializes games list.
     *
     * @param Collection $games A Collection of Games.
     *
     * @return array
     */
    public function serializeList(Collection $games): array
    {
        $serializedGames = [];

        foreach ($games as $game) {
            $serializedGame = [];

            $serializedGame['uuid'] = $game->uuid;
            $serializedGame['name'] = $game->name;
            $serializedGame['created_at'] = $game->created_at;
            $serializedGame['updated_at'] = $game->updated_at;

            $serializedGames[] = $serializedGame;
        }

        return $serializedGames;
    }

    /**
     * Serialize single Game model.
     *
     * @param Game $game A Game model instance.
     *
     * @return array
     */
    public function serializeSingle(Game $game): array
    {
        return [
            'uuid' => $game->uuid,
            'name' => $game->name,
            'created_at' => $game->created_at,
            'updated_at' => $game->updated_at,
        ];
    }
}
