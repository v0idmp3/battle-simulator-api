<?php

declare(strict_types=1);

namespace App\Games\Game;

use App\Games\Game;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class Repository
{
    /**
     * A Game model instance.
     *
     * @var Game
     */
    private $gameModel;

    /**
     * @param Game $gameModel A Game model instance.
     */
    public function __construct(Game $gameModel)
    {
        $this->gameModel = $gameModel;
    }

    /**
     * Try to find a game by uuid.
     *
     * @param string $gameUuid A Game uuid string.
     *
     * @return Game|null
     */
    public function findByUuid(string $gameUuid): ?Game
    {
        return $this->gameModel->where('uuid', $gameUuid)->first();
    }

    /**
     * Fetch all games ordered by created at time.
     *
     * @return Collection
     */
    public function getAllWithOrder(): Collection
    {
        return $this->gameModel->orderByDesc('created_at')->get();
    }

    /**
     * Create a new game and returns it.
     *
     * @param array $inputData The input data.
     *
     * @return Game
     */
    public function create(array $inputData): Game
    {
        $game = $this->gameModel->newInstance();

        $game->uuid = Arr::get($inputData, 'uuid', null);
        $game->name = Arr::get($inputData, 'name', null);

        $game->save();

        return $game;
    }
}
