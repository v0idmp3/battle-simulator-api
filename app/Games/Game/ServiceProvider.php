<?php

declare(strict_types=1);

namespace App\Games\Game;

use App\Games\Game;
use App\Games\Game\Repository as GameRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\Registrar as RegistrarContract;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Ramsey\Uuid\Uuid;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * @inheritDoc
     */
    public function boot(): void
    {
        $this->registerRoutePatterns($this->app[Router::class], $this->app);
        $this->registerApiRoutes($this->app[Router::class]);
    }

    /**
     * Registers the route patterns.
     *
     * @param RegistrarContract $router A Router instance.
     * @param Application       $app    The Application instance.
     *
     * @throws ModelNotFoundException
     *
     * @return void
     */
    private function registerRoutePatterns(RegistrarContract $router, Application $app): void
    {
        $router->pattern('gameUuid', Uuid::VALID_PATTERN);

        $router->bind('gameUuid', static function (string $value) use ($app): Game {
            $gameRepository = $app->make(GameRepository::class);
            $game = $gameRepository->findByUuid($value);

            if (!$game) {
                throw new ModelNotFoundException('Game not found.');
            }

            return $game;
        });
    }

    /**
     * Registers API routes.
     *
     * @param RegistrarContract $router A Router instance.
     *
     * @return void
     */
    private function registerApiRoutes(RegistrarContract $router): void
    {
        $attributes = [
            'prefix' => 'api/v1/games',
            'middleware' => ['api'],
            'namespace' => 'App\Games\Http\Controllers\Api\V1\Game',
        ];

        $router->group($attributes, static function (RegistrarContract $router): void {
            $router->get('', 'Controller@index');
            $router->post('', 'Controller@store');

            $router->post('{gameUuid}/actions/attack', 'Controller@attack');
            $router->get('{gameUuid}/actions/logs', 'Controller@logs');
            $router->post('{gameUuid}/actions/reset', 'Controller@resetGame');
        });

        $attributes = [
            'prefix' => 'api/v1/armies',
            'middleware' => ['api'],
            'namespace' => 'App\Games\Http\Controllers\Api\V1\Army',
        ];

        $router->group($attributes, static function (RegistrarContract $router): void {
            $router->get('{gameUuid}', 'Controller@index');
        });
    }
}
