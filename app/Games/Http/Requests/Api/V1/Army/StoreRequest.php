<?php

declare(strict_types=1);

namespace App\Games\Http\Requests\Api\V1\Army;

use App\Games\Army\StrategyType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'game_uuid' => [
                'required',
                'exists:games,uuid',
                'string',
            ],
            'name' => [
                'required',
                'string',
            ],
            'total_units' => [
                'required',
                'integer',
                'min:80',
                'max:100',
            ],
            'strategy_type' => [
                'required',
                'string',
                Rule::in([
                    StrategyType::RANDOM,
                    StrategyType::WEAKEST,
                    StrategyType::STRONGEST,
                ]),
            ],
        ];
    }
}
