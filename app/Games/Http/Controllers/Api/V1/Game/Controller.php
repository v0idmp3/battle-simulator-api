<?php

declare(strict_types=1);

namespace App\Games\Http\Controllers\Api\V1\Game;

use App\Games\Attack\LogService as AttackLogService;
use App\Games\Attack\Serializer as AttackSerializer;
use App\Games\Attack\Service as AttackService;
use App\Games\Exceptions\AttackException;
use App\Games\Game;
use App\Games\Game\Serializer as GameSerializer;
use App\Games\Game\Service as GameService;
use App\Games\Http\Requests\Api\V1\Game\StoreRequest;
use App\Http\Controllers\BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    /**
     * Returns the list of games.
     *
     * @param GameService    $gameService    A GameService instance.
     * @param GameSerializer $gameSerializer A GameSerializer instance.
     *
     * @return JsonResponse
     */
    public function index(GameService $gameService, GameSerializer $gameSerializer): JsonResponse
    {
        $games = $gameService->getAll();

        return JsonResponse::create([
            'games' => $gameSerializer->serializeList($games),
        ]);
    }

    /**
     * Create a new game.
     *
     * @param StoreRequest   $request        A StoreRequest instance.
     * @param GameService    $gameService    A GameService instance.
     * @param GameSerializer $gameSerializer A GameSerializer instance.
     *
     * @return JsonResponse
     */
    public function store(StoreRequest $request, GameService $gameService, GameSerializer $gameSerializer): JsonResponse
    {
        $newGame = $gameService->createNewGame($request->get('name'));

        return JsonResponse::create($gameSerializer->serializeSingle($newGame));
    }

    /**
     * Execute an attack.
     *
     * @param Game             $game             The Game instance.
     * @param AttackService    $attackService    An AttackService instance.
     * @param AttackSerializer $attackSerializer An AttackSerializer instance.
     *
     * @return JsonResponse
     */
    public function attack(Game $game, AttackService $attackService, AttackSerializer $attackSerializer): JsonResponse
    {
        try {
            $attack = $attackService->executeAttack($game);
        } catch (AttackException $exception) {
            return JsonResponse::create([
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ], 400);
        }

        return JsonResponse::create($attackSerializer->serializeSingleAttack($attack));
    }

    /**
     * Returns a list of attacks for game log.
     *
     * @param Game             $game             The Game instance.
     * @param AttackLogService $attackLogService An AttackLogService instance.
     * @param AttackSerializer $attackSerializer An AttackSerializer instance.
     *
     * @return JsonResponse
     */
    public function logs(Game $game, AttackLogService $attackLogService, AttackSerializer $attackSerializer): JsonResponse
    {
        $attacks = $attackLogService->getAllAttacksFromGame($game);

        return JsonResponse::create([
            'attacks' => $attackSerializer->serializeList($attacks),
        ]);
    }

    /**
     * Reset a game.
     *
     * @param Game        $game        The Game instance.
     * @param GameService $gameService A GameService instance.
     *
     * @return JsonResponse
     */
    public function resetGame(Game $game, GameService $gameService): JsonResponse
    {
        $gameService->resetGame($game);

        return JsonResponse::create([]);
    }
}
