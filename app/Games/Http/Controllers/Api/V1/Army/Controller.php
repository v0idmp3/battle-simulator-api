<?php

declare(strict_types=1);

namespace App\Games\Http\Controllers\Api\V1\Army;

use App\Games\Army\Serializer as ArmySerializer;
use App\Games\Army\Service as ArmyService;
use App\Games\Game;
use App\Games\Http\Requests\Api\V1\Army\StoreRequest;
use App\Http\Controllers\BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    /**
     * Get all armies for the given game instance.
     *
     * @param Game           $game           The Game model instance.
     * @param ArmyService    $armyService    An ArmyService instance.
     * @param ArmySerializer $armySerializer An ArmySerializer instance.
     *
     * @return JsonResponse
     */
    public function index(Game $game, ArmyService $armyService, ArmySerializer $armySerializer): JsonResponse
    {
        $armies = $armyService->getAllArmiesInTheGame($game);

        return JsonResponse::create([
            'armies' => $armySerializer->serializeList($armies),
        ]);
    }

    /**
     * Create and return new army.
     *
     * @param StoreRequest   $request        A StoreRequest instance.
     * @param ArmyService    $armyService    An ArmyService instance.
     * @param ArmySerializer $armySerializer An ArmySerializer instance.
     *
     * @return JsonResponse
     */
    public function store(StoreRequest $request, ArmyService $armyService, ArmySerializer $armySerializer): JsonResponse
    {
        $newArmy = $armyService->createNewArmy(
            $request->get('game_uuid'),
            $request->get('name'),
            (int) $request->get('total_units'),
            $request->get('strategy_type'),
        );

        return JsonResponse::create($armySerializer->serializeSingle($newArmy));
    }
}
