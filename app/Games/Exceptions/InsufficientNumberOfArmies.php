<?php

declare(strict_types=1);

namespace App\Games\Exceptions;

use App\Games\Exceptions\AttackException;
use RuntimeException;
use Throwable;

class InsufficientNumberOfArmies extends RuntimeException implements AttackException
{
    /**
     * @param Throwable|null $previous The previous Exception.
     */
    public function __construct(?Throwable $previous = null)
    {
        $code = 2;
        parent::__construct('Insufficient number of armies in the game. Minimum is 5 armies.', $code, $previous);
    }
}
