<?php

declare(strict_types=1);

namespace App\Games\Exceptions;

use App\Games\Exceptions\AttackException;
use RuntimeException;
use Throwable;

class GameIsOver extends RuntimeException implements AttackException
{
    /**
     * @param Throwable|null $previous The previous Exception.
     */
    public function __construct(?Throwable $previous = null)
    {
        $code = 1;
        parent::__construct('Game is over.', $code, $previous);
    }
}
