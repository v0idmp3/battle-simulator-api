<?php

declare(strict_types=1);

namespace App\Games\Exceptions;

use App\Games\Exceptions\AttackException;
use RuntimeException;
use Throwable;

class UnrecognizedStrategyType extends RuntimeException implements AttackException
{
    /**
     * @param string         $strategyType Unrecognized strategy type.
     * @param Throwable|null $previous     The previous Exception.
     */
    public function __construct(string $strategyType, ?Throwable $previous = null)
    {
        $code = 3;
        parent::__construct('Unrecognized strategy type: ' . $strategyType, $code, $previous);
    }
}
