<?php

declare(strict_types=1);

namespace App\Games\Army;

use App\Games\Army;
use App\Games\Army\Repository as ArmyRepository;
use App\Games\Game;
use App\Games\Game\Repository as GameRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Ramsey\Uuid\Uuid;

class Service
{
    /**
     * An ArmyRepository instance.
     *
     * @var ArmyRepository
     */
    private $armyRepository;

    /**
     * A GameRepository instance.
     *
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * @param ArmyRepository $armyRepository A ArmyRepository instance.
     * @param GameRepository $gameRepository A GameRepository instance.
     */
    public function __construct(ArmyRepository $armyRepository, GameRepository $gameRepository)
    {
        $this->armyRepository = $armyRepository;
        $this->gameRepository = $gameRepository;
    }

    /**
     * Returns all games in database.
     *
     * @param Game $game The Game model instance.
     *
     * @return Collection
     */
    public function getAllArmiesInTheGame(Game $game): Collection
    {
        $armies = $this->armyRepository->getAllGameArmiesWithOrder($game->id);
        $armies->load('game');

        return $armies;
    }

    /**
     * Create a new army and returns it.
     *
     * @param string $gameUuid     The Game uuid string.
     * @param string $name         An Army name.
     * @param int    $totalUnits   A total units count.
     * @param string $strategyType An army strategy type string.
     *
     * @throws ModelNotFoundException
     *
     * @return Army
     */
    public function createNewArmy(string $gameUuid, string $name, int $totalUnits, string $strategyType): Army
    {
        $game = $this->gameRepository->findByUuid($gameUuid);

        if (!$game) {
            throw new ModelNotFoundException('Game not found');
        }

        $inputData = [
            'game_id' => $game->id,
            'uuid' => Uuid::uuid4(),
            'name' => $name,
            'strategy_type' => $strategyType,
            'total_units' => $totalUnits,
        ];

        $createdArmy = $this->armyRepository->create($inputData);
        $createdArmy->load('game');

        return $createdArmy;
    }
}
