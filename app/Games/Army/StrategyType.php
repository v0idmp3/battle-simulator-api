<?php

declare(strict_types=1);

namespace App\Games\Army;

interface StrategyType
{
    /**
     * Army with RANDOM strategy type will attack random army in a game.
     */
    public const RANDOM = 'random';

    /**
     * Army with WEAKEST strategy type will attack army with smallest
     * number of units in a game.
     */
    public const WEAKEST = 'weakest';

    /**
     * Army with STRONGEST strategy type will attack army with biggest
     * number of units in a game.
     */
    public const STRONGEST = 'strongest';
}
