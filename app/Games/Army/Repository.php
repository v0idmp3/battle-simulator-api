<?php

declare(strict_types=1);

namespace App\Games\Army;

use App\Games\Army;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class Repository
{
    /**
     * An Army model instance.
     *
     * @var Army
     */
    private $armyModel;

    /**
     * @param Army $armyModel An Army model instance.
     */
    public function __construct(Army $armyModel)
    {
        $this->armyModel = $armyModel;
    }

    /**
     * Try to find a army by uuid.
     *
     * @param string $armyUuid An Army uuid string.
     *
     * @return Army|null
     */
    public function findByUuid(string $armyUuid): ?Army
    {
        return $this->armyModel->where('uuid', $armyUuid)->first();
    }

    /**
     * Fetch all armies ordered by created at time.
     *
     * @param int $gameId A Game ID.
     *
     * @return Collection
     */
    public function getAllGameArmiesWithOrder(int $gameId): Collection
    {
        return $this->armyModel->where('game_id', $gameId)->orderByDesc('created_at')->get();
    }

    /**
     * Fetch all alive armies ordered by created at time.
     *
     * @param int $gameId A Game ID.
     *
     * @return Collection
     */
    public function getAllAliveArmiesWithOrder(int $gameId): Collection
    {
        return $this->armyModel->where('game_id', $gameId)
            ->whereColumn('total_units', '>', 'killed_units')
            ->orderByDesc('created_at')
            ->get();
    }

    /**
     * Count all armies in the game.
     *
     * @param int $gameId The game id.
     *
     * @return int
     */
    public function countArmiesInGame(int $gameId): int
    {
        return $this->armyModel->where('game_id', $gameId)->count();
    }

    /**
     * Create a new army and returns it.
     *
     * @param array $inputData The input data.
     *
     * @return Army
     */
    public function create(array $inputData): Army
    {
        $army = $this->armyModel->newInstance();

        $army->game_id = Arr::get($inputData, 'game_id', null);
        $army->uuid = Arr::get($inputData, 'uuid', null);
        $army->name = Arr::get($inputData, 'name', null);
        $army->strategy_type = Arr::get($inputData, 'strategy_type', null);
        $army->total_units = Arr::get($inputData, 'total_units', null);

        $army->save();

        return $army;
    }

    /**
     * Updates passed Army instance with the input data.
     *
     * @param Army  $army      An Army instance.
     * @param array $inputData The input data.
     *
     * @return Army
     */
    public function update(Army $army, array $inputData): Army
    {
        $army->killed_units = Arr::get($inputData, 'killed_units', $army->killed_units);
        $army->reload_time_in_milliseconds = Arr::get($inputData, 'reload_time_in_milliseconds', $army->reload_time_in_milliseconds);

        $army->save();

        return $army;
    }

    /**
     * Reset killed units count and unset reload time.
     *
     * @param array $armyIds An array of army IDs.
     *
     * @return void
     */
    public function resetArmies(array $armyIds): void
    {
        $this->armyModel->whereIn('id', $armyIds)->update([
            'killed_units' => 0,
            'reload_time_in_milliseconds' => null,
        ]);
    }
}
