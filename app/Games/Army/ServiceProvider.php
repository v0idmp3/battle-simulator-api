<?php

declare(strict_types=1);

namespace App\Games\Army;

use Illuminate\Contracts\Routing\Registrar as RegistrarContract;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * @inheritDoc
     */
    public function boot(): void
    {
        $this->registerApiRoutes($this->app[Router::class]);
    }

    /**
     * Registers API routes.
     *
     * @param RegistrarContract $router A Router instance.
     *
     * @return void
     */
    private function registerApiRoutes(RegistrarContract $router): void
    {
        $attributes = [
            'prefix' => 'api/v1/armies',
            'middleware' => ['api'],
            'namespace' => 'App\Games\Http\Controllers\Api\V1\Army',
        ];

        $router->group($attributes, static function (RegistrarContract $router): void {
            $router->post('', 'Controller@store');
        });
    }
}
