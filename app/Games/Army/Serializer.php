<?php

declare(strict_types=1);

namespace App\Games\Army;

use App\Games\Army;
use Illuminate\Database\Eloquent\Collection;

class Serializer
{
    /**
     * Serializes armies list.
     *
     * @param Collection $armies A Collection of Army models.
     *
     * @return array
     */
    public function serializeList(Collection $armies): array
    {
        $serializedArmies = [];

        foreach ($armies as $army) {
            $serializedArmy = [];

            $serializedArmy['uuid'] = $army->uuid;
            $serializedArmy['game_uuid'] = $army->game->uuid;
            $serializedArmy['name'] = $army->name;
            $serializedArmy['total_units'] = $army->total_units;
            $serializedArmy['killed_units'] = $army->killed_units;
            $serializedArmy['strategy_type'] = $army->strategy_type;
            $serializedArmy['created_at'] = $army->created_at;
            $serializedArmy['updated_at'] = $army->updated_at;

            $serializedArmies[] = $serializedArmy;
        }

        return $serializedArmies;
    }

    /**
     * Serialize single Army model.
     *
     * @param Army $army A Army model instance.
     *
     * @return array
     */
    public function serializeSingle(Army $army): array
    {
        return [
            'uuid' => $army->uuid,
            'game_uuid' => $army->game->uuid,
            'name' => $army->name,
            'total_units' => $army->total_units,
            'killed_units' => $army->killed_units,
            'strategy_type' => $army->strategy_type,
            'created_at' => $army->created_at,
            'updated_at' => $army->updated_at,
        ];
    }
}
