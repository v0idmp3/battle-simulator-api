<?php

declare(strict_types=1);

namespace App\Games;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Game extends Model
{
    /**
     * Eloquent relationship: Each game may have many armies.
     *
     * @return HasMany
     */
    public function armies(): HasMany
    {
        return $this->hasMany(Army::class, 'game_id', 'id');
    }
}
