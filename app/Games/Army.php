<?php

declare(strict_types=1);

namespace App\Games;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Army extends Model
{
    /**
     * @inheritDoc
     */
    protected $appends = [
        'alive_units',
    ];

    /**
     * Set alive units attribute.
     *
     * @return int
     */
    public function getAliveUnitsAttribute(): int
    {
        return $this->total_units - $this->killed_units;
    }

    /**
     * Eloquent relationship: Each army belongs to the game.
     *
     * @return BelongsTo
     */
    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class, 'game_id');
    }

    /**
     * Eloquent relationship: Each army may have many attacks.
     *
     * @return HasMany
     */
    public function attacks(): HasMany
    {
        return $this->hasMany(Attack::class, 'attacker_army_id', 'id');
    }
}
