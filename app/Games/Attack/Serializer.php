<?php

declare(strict_types=1);

namespace App\Games\Attack;

use App\Games\Army\Serializer as ArmySerializer;
use App\Games\Attack;
use Illuminate\Database\Eloquent\Collection;

class Serializer
{
    /**
     * An ArmySerializer instance.
     *
     * @var ArmySerializer
     */
    private $armySerializer;

    /**
     * @param ArmySerializer $armySerializer An ArmySerializer instance.
     */
    public function __construct(ArmySerializer $armySerializer)
    {
        $this->armySerializer = $armySerializer;
    }

    /**
     * Serialize single Attack model.
     *
     * @param Attack $attack A Attack model instance.
     *
     * @return array
     */
    public function serializeSingleAttack(Attack $attack): array
    {
        $serializedAttack = [
            'id' => $attack->id,
            'attacker_army' => $this->armySerializer->serializeSingle($attack->attackerArmy),
            'is_attack_successful' => false,
        ];

        if ($attack->defendingArmy) {
            $serializedAttack['is_attack_successful'] = true;
            $serializedAttack['defending_army'] = $this->armySerializer->serializeSingle($attack->defendingArmy);
            $serializedAttack['attack_strength'] = $attack->attack_strength;
            $serializedAttack['killed_units'] = $attack->killed_units;
            $serializedAttack['reload_time_in_milliseconds'] = $attack->attackerArmy->reload_time_in_milliseconds;
        }

        return $serializedAttack;
    }

    /**
     * Serialize a list of attacks for game log.
     *
     * @param Collection $attacks A Collection of attacks.
     *
     * @return array
     */
    public function serializeList(Collection $attacks): array
    {
        $serializedAttacks = [];

        foreach ($attacks as $attack) {
            $serializedAttacks[] = $this->serializeSingleAttack($attack);
        }

        return $serializedAttacks;
    }
}
