<?php

declare(strict_types=1);

namespace App\Games\Attack;

use App\Games\Attack\Repository as AttackRepository;
use App\Games\Game;
use Illuminate\Database\Eloquent\Collection;

class LogService
{
    /**
     * An AttackRepository instance.
     *
     * @var AttackRepository
     */
    private $attackRepository;

    /**
     * @param AttackRepository $attackRepository An AttackRepository instance.
     */
    public function __construct(AttackRepository $attackRepository)
    {
        $this->attackRepository = $attackRepository;
    }

    /**
     * Returns all attacks by armies in the game.
     *
     * @param Game $game The Game instance.
     *
     * @return Collection
     */
    public function getAllAttacksFromGame(Game $game): Collection
    {
        // Load relation.
        $game->load('armies');

        // Pluck army IDs from the game.
        $armyIds = $game->armies->pluck('id')->toArray();

        $attacks = $this->attackRepository->getAllAttacksByArmies($armyIds);

        $attacks->load([
            'attackerArmy',
            'defendingArmy',
        ]);

        return $attacks;
    }
}
