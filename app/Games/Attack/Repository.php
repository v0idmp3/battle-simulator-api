<?php

declare(strict_types=1);

namespace App\Games\Attack;

use App\Games\Attack;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class Repository
{
    /**
     * An Attack model instance.
     *
     * @var Attack
     */
    private $attackModel;

    /**
     * @param Attack $attackModel An Attack model instance.
     */
    public function __construct(Attack $attackModel)
    {
        $this->attackModel = $attackModel;
    }

    /**
     * Get last attack from armies.
     *
     * @param array $armyIds An array of army ids.
     *
     * @return Attack|null
     */
    public function getLastAttackFromArmies(array $armyIds): ?Attack
    {
        return $this->attackModel->whereIn('attacker_army_id', $armyIds)->orderByDesc('id')->first();
    }

    /**
     * Create new attack.
     *
     * @param array $inputData The input data.
     *
     * @return Attack
     */
    public function create(array $inputData): Attack
    {
        $attack = $this->attackModel->newInstance();

        $attack->attacker_army_id = Arr::get($inputData, 'attacker_army_id', null);
        $attack->defending_army_id = Arr::get($inputData, 'defending_army_id', null);
        $attack->attack_strength = Arr::get($inputData, 'attack_strength', null);
        $attack->killed_units = Arr::get($inputData, 'killed_units', null);

        $attack->save();

        return $attack;
    }

    /**
     * Get all armies attacks.
     *
     * @param array $armyIds An array of army IDs.
     *
     * @return Collection
     */
    public function getAllAttacksByArmies(array $armyIds): Collection
    {
        return $this->attackModel->whereIn('attacker_army_id', $armyIds)->orderBy('id')->get();
    }

    /**
     * Deletes all armies attacks.
     *
     * @param array $armyIds An array of army Ids.
     *
     * @return void
     */
    public function deleteAttacksByArmies(array $armyIds): void
    {
        $this->attackModel->whereIn('attacker_army_id', $armyIds)->delete();
    }
}
