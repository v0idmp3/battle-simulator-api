<?php

declare(strict_types=1);

namespace App\Games\Attack;

use App\Games\Army;
use App\Games\Army\Repository as ArmyRepository;
use App\Games\Army\StrategyType;
use App\Games\Attack;
use App\Games\Attack\Repository as AttackRepository;
use App\Games\Exceptions\GameIsOver;
use App\Games\Exceptions\InsufficientNumberOfArmies;
use App\Games\Exceptions\UnrecognizedStrategyType;
use App\Games\Game;
use Illuminate\Database\Eloquent\Collection;

class Service
{
    /**
     * The minimum number of armies in the game for
     * executing attack.
     */
    private const MIN_NUMBER_OF_ARMIES = 5;

    /**
     * Default strength of attack per unit.
     */
    private const DEFAULT_STRENGTH_PER_UNIT = 0.5;

    /**
     * Default strength of attack when army have only one unit left.
     */
    private const DEFAULT_STRENGTH_FOR_ONE_UNIT = 1;

    /**
     * Default reload time per unit in milliseconds.
     */
    private const RELOAD_TIME_PER_UNIT = 10;

    /**
     * An ArmyRepository instance.
     *
     * @var ArmyRepository
     */
    private $armyRepository;

    /**
     * An AttackRepository instance.
     *
     * @var AttackRepository
     */
    private $attackRepository;

    /**
     * @param ArmyRepository   $armyRepository   An ArmyRepository instance.
     * @param AttackRepository $attackRepository An AttackRepository instance.
     */
    public function __construct(ArmyRepository $armyRepository, AttackRepository $attackRepository)
    {
        $this->armyRepository = $armyRepository;
        $this->attackRepository = $attackRepository;
    }

    /**
     * Try to execute an attack.
     *
     * @param Game $game The Game instance.
     *
     * @throws InsufficientNumberOfArmies
     * @throws GameIsOver
     * @throws UnrecognizedStrategyType
     *
     * @return Attack
     */
    public function executeAttack(Game $game): Attack
    {
        // Check if we have enough number of armies in the game.
        if ($this->armyRepository->countArmiesInGame($game->id) < self::MIN_NUMBER_OF_ARMIES) {
            throw new InsufficientNumberOfArmies();
        }

        // Get all alive armies in order.
        $aliveArmies = $this->armyRepository->getAllAliveArmiesWithOrder($game->id);
        $aliveArmies->load('attacks');

        // Check if game is already over.
        if ($aliveArmies->count() === 1) {
            throw new GameIsOver();
        }

        // Find attacker army.
        $attackerArmy = $this->findAttackingArmy($aliveArmies);

        // Decide if attack is successful or not.
        $isAttackSuccessful = $this->isAttackSuccessful($attackerArmy);

        if (!$isAttackSuccessful) {
            return $this->createUnsuccessfulAttack($attackerArmy);
        }

        // Calculate the attack strength.
        $attackStrength = $this->calculateAttackStrength($attackerArmy);

        // Create a collection of possible defending armies from all alive armies
        // without army which is selected as an attacker army.
        $possibleDefendingArmies = $aliveArmies->filter(static function (Army $army) use ($attackerArmy) {
            return $army->id !== $attackerArmy->id;
        });

        // Select defending army based on attacker army strategy type.
        switch ($attackerArmy->strategy_type) {
            case StrategyType::RANDOM:
                $defendingArmy = $this->getRandomDefendingArmy($possibleDefendingArmies);
                break;
            case StrategyType::STRONGEST:
                $defendingArmy = $this->getStrongestDefendingArmy($possibleDefendingArmies);
                break;
            case StrategyType::WEAKEST:
                $defendingArmy = $this->getWeakestDefendingArmy($possibleDefendingArmies);
                break;
            default:
                throw new UnrecognizedStrategyType($attackerArmy->strategy_type);
                break;
        }

        $killedUnitsCount = $this->calculateCasualties($defendingArmy, $attackStrength);

        // Create an input array for successful attack.
        $attackInputData = [
            'attacker_army_id' => $attackerArmy->id,
            'defending_army_id' => $defendingArmy->id,
            'attack_strength' => $attackStrength,
            'killed_units' => $killedUnitsCount,
        ];

        // Create an attack.
        $attack = $this->attackRepository->create($attackInputData);

        // Calculate attacker army reload time.
        $reloadTimeInMilliseconds = $this->calculateReloadTime($attackerArmy->alive_units);

        // Update attacker army reload time.
        $this->armyRepository->update($attackerArmy, [
            'reload_time_in_milliseconds' => $reloadTimeInMilliseconds,
        ]);

        // Check whether defending army is defeated or not.
        if ($defendingArmy->alive_units <= $killedUnitsCount) {
            $totalKilledUnits = $defendingArmy->total_units;
        } else {
            $totalKilledUnits = $defendingArmy->killed_units + $killedUnitsCount;
        }

        // Update defending army with casualties.
        $this->armyRepository->update($defendingArmy, [
            'killed_units' => $totalKilledUnits,
        ]);

        // Load relations.
        $attack->load([
            'attackerArmy',
            'defendingArmy',
        ]);

        return $attack;
    }

    /**
     * Find next attacking army from a collection of alive armies.
     *
     * @param Collection $armies A collection of armies.
     *
     * @return Army
     */
    private function findAttackingArmy(Collection $armies): Army
    {
        // Get army IDs from the collection.
        $armyIds = $armies->pluck('id')->toArray();

        // Try to fetch last attack from the one of armies.
        $lastAttack = $this->attackRepository->getLastAttackFromArmies($armyIds);

        // If last attack is not found that means our first army
        // in the collection is our attacker army.
        if ($lastAttack === null) {
            return $armies->first();
        }

        // Load relation.
        $lastAttack->load('attackerArmy');

        $lastAttackerArmy = $lastAttack->attackerArmy;

        // Find a position of army which attacked in previous attack.
        $lastAttackerArmyPosition = $armies->search(static function (Army $army) use ($lastAttackerArmy): bool {
            return $army->id === $lastAttackerArmy->id;
        });

        // If army position is not last, return next army from collection.
        if (($armies->count() - 1) > $lastAttackerArmyPosition) {
            return $armies->get($lastAttackerArmyPosition + 1);
        }

        return $armies->first();
    }

    /**
     * Decide whether the attack is gonna be successful or not.
     *
     * @param Army $attackerArmy An Army instance.
     *
     * @return bool
     */
    private function isAttackSuccessful(Army $attackerArmy): bool
    {
        return rand(1, 100) <= $attackerArmy->alive_units;
    }

    /**
     * Calculate attack strength.
     *
     * @param Army $attackerArmy An Army instance.
     *
     * @return int
     */
    private function calculateAttackStrength(Army $attackerArmy): int
    {
        if ($attackerArmy->alive_units === 1) {
            return self::DEFAULT_STRENGTH_FOR_ONE_UNIT;
        }

        return (int) ($attackerArmy->alive_units * self::DEFAULT_STRENGTH_PER_UNIT);
    }

    /**
     * Returns random army as a defending army.
     *
     * @param Collection $armies A collection of armies.
     *
     * @return Army
     */
    private function getRandomDefendingArmy(Collection $armies): Army
    {
        return $armies->random();
    }

    /**
     * Returns the strongest army as a defending army.
     *
     * @param Collection $armies A collection of armies.
     *
     * @return Army
     */
    private function getStrongestDefendingArmy(Collection $armies): Army
    {
        return $armies->sortByDesc('alive_units')->first();
    }

    /**
     * Returns the weakest army as a defending army.
     *
     * @param Collection $armies A collection of armies.
     *
     * @return Army
     */
    private function getWeakestDefendingArmy(Collection $armies): Army
    {
        return $armies->sortBy('alive_units')->first();
    }

    /**
     * Creates an unsuccessful attack.
     *
     * @param Army $attackerArmy An Army instance.
     *
     * @return Attack
     */
    private function createUnsuccessfulAttack(Army $attackerArmy): Attack
    {
        return $this->attackRepository->create([
            'attacker_army_id' => $attackerArmy->id,
        ]);
    }

    /**
     * Calculates a reload time for the given number of alive units.
     *
     * @param int $aliveUnits The number of alive units.
     *
     * @return int
     */
    private function calculateReloadTime(int $aliveUnits): int
    {
        return $aliveUnits * self::RELOAD_TIME_PER_UNIT;
    }

    /**
     * Calculate casualties based on attack strength.
     *
     * @param Army $defendingArmy  An Army model instance.
     * @param int  $attackStrength An attack strength.
     *
     * @return int
     */
    private function calculateCasualties(Army $defendingArmy, int $attackStrength): int
    {
        return $defendingArmy->alive_units >= $attackStrength ? $attackStrength : $defendingArmy->alive_units;
    }
}
