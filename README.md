## Battle Simulator API

The Battle Simulator API for job testing.

### Installation

- Run `composer install`.
- Copy `.env.example` to `.env` file and change default settings.
- Run migrations with `php artisan migrate --step`.

### Documentation

Postman documentation for this API can be found here: [Documentation](https://documenter.getpostman.com/view/1358276/SzS1Snxc?version=latest).
