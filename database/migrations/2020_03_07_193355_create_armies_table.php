<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArmiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('armies', static function (Blueprint $table): void {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('game_id');
            $table->uuid('uuid');
            $table->string('name');
            $table->string('strategy_type');
            $table->integer('total_units');
            $table->integer('killed_units')->default(0);
            $table->timestamp('reload_finish_at')->nullable();
            $table->timestamps();

            $table->index('game_id');
            $table->foreign('game_id')->references('id')->on('games')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('armies');
    }
}
