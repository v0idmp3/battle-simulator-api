<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('attacks', static function (Blueprint $table): void {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('attacker_army_id');
            $table->unsignedBigInteger('defending_army_id')->nullable();
            $table->unsignedInteger('attack_strength')->nullable();
            $table->integer('killed_units')->nullable();
            $table->timestamps();

            $table->index('attacker_army_id');
            $table->foreign('attacker_army_id')->references('id')->on('armies')->onUpdate('cascade')->onDelete('cascade');

            $table->index('defending_army_id');
            $table->foreign('defending_army_id')->references('id')->on('armies')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('attacks');
    }
}
