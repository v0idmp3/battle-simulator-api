<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterArmiesTableAddReloadTimeInMillisecondsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('armies', static function (Blueprint $table): void {
            $table->dropColumn('reload_finish_at');
            $table->integer('reload_time_in_milliseconds')->nullable()->after('killed_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('armies', static function (Blueprint $table): void {
            $table->dropColumn('reload_time_in_milliseconds');
            $table->timestamp('reload_finish_at')->nullable()->after('killed_units');
        });
    }
}
